import 'package:flutter/material.dart';
import '../pages_elements/containerBlock.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../pages_elements/icon_content.dart';
import '../constants.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

enum Gender { male, female }

class _InputPageState extends State<InputPage> {
  Color maleCardColor = kInactiveCardColor;
  Color femaleCardColor = kInactiveCardColor;

  Gender selectedGender;

  int height = 180;
  int weight = 60;
  int age = 20;

  void updateColor(Gender selectedGender) {
    selectedGender == Gender.female
        ? femaleCardColor = kActiveCardColor
        : femaleCardColor = kInactiveCardColor;
    selectedGender == Gender.male
        ? maleCardColor = kActiveCardColor
        : maleCardColor = kInactiveCardColor;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      backgroundColor: Color(0xFF00AABB),
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
//        backgroundColor: Color(0xFF00AABB),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        updateColor(Gender.male);
                      });
                      print('MALE button pressed');
                    },
                    child: ReusableCard(
                      color: maleCardColor,
                      cardChild: IconContent(
                        label: 'MALE',
                        icon: FontAwesomeIcons.mars,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        updateColor(Gender.female);
                      });
                      print('FEMALE button pressed');
                    },
                    child: ReusableCard(
                      color: femaleCardColor,
                      cardChild: IconContent(
                        label: 'FEMALE',
                        icon: FontAwesomeIcons.venus,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ReusableCard(
              color: Color(0xFFAABBCC),
              cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'HEIGHT ',
                    style: kLabelTextStyle,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: <Widget>[
                      Text(
                        height.toString(),
                        style: kNumberTextStyle,
                      ),
                      Text(
                        'cm',
                        style: kLabelTextStyle,
                      ),
                    ],
                  ),
                  SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                      thumbColor: Color(0xFFEB1555),
                      overlayColor: Color(0x29EB1555),
                      activeTrackColor: Colors.white,
                      inactiveTrackColor: Color(0xFFB1555),
                      thumbShape:
                          RoundSliderThumbShape(enabledThumbRadius: 15.0),
                      overlayShape:
                          RoundSliderOverlayShape(overlayRadius: 20.0),
                    ),
                    child: Slider(
                      value: height.toDouble(),
                      min: kSliderValueMin,
                      max: kSliderValueMax,
                      //activeColor: Color(0xFF8D8E98), //replaced with white and moved level up to SliderTheme as activeTrackColor
                      //inactiveColor: Color(0xFFB1555), //as above moved level up to SliderTheme as inactiveTrackColor
                      onChanged: (double newValue) {
                        setState(() {
                          print(newValue);
                          height = newValue
                              .round(); // .round() will make int from double
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ReusableCard(
                    color: kInactiveCardColor,
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'WEIGHT',
                          style: kLabelTextStyle,
                        ),
                        Text(
                          weight.toString(),
                          style: kNumberTextStyle,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            //replaced with our custom button CustomRoundButton
                            CustomRoundIconButton(
                              icon: FontAwesomeIcons.plus,
                              onPressed: () {
                                setState(() {
                                  weight++;
                                });
                              },
                            ),
//                            FloatingActionButton(
//                              onPressed: null,
//                              child: Icon(
//                                Icons.add,
//                                color: Colors.white,
//                              ),
//                            ),
                            SizedBox(
                              //for cteating space between other widgets
                              width: 10.0,
                            ),
                            //as above we replace this with our custom button
                            CustomRoundIconButton(
                              icon: FontAwesomeIcons.minus,
                              onPressed: () {
                                setState(() {
                                  weight--;
                                });
                              },
                            )
//                            FloatingActionButton(
//                              onPressed: null,
//                              child: Icon(
//                                Icons.add,
//                                color: Colors.white,
//                              ),
//                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: ReusableCard(
                    color: kInactiveCardColor,
                    cardChild: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'AGE',
                          style: kLabelTextStyle,
                        ),
                        Text(
                          age.toString(),
                          style: kNumberTextStyle,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CustomRoundIconButton(
                                icon: FontAwesomeIcons.plus,
                                onPressed: () {
                                  setState(() {
                                    age++;
                                  });
                                }),
                            SizedBox(width: 10.0),
                            CustomRoundIconButton(
                                icon: FontAwesomeIcons.minus,
                                onPressed: () {
                                  setState(() {
                                    age--;
                                  });
                                }),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            color: kBottomContainerColor,
//            margin:
//                EdgeInsets.only(left: 10.0, top: 0.0, right: 10.0, bottom: 0.0),
            //width: ,
            height: kBottomContainerHeight,
          ),
        ],
      ),
    );
  }
}

class CustomRoundIconButton extends StatelessWidget {
  //adding ability to have child
  //final Widget child; //could set it up like this but because our widget is icon we can:
  final IconData icon; //and use it in constructor and as a child
  final Function onPressed; // to handle taps

  //constructor
  CustomRoundIconButton({@required this.icon, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      shape: CircleBorder(),
      fillColor: kScaffoldBackgroundColor,
      constraints: BoxConstraints.tightFor(
        //this is taken from FAB
        width: 56.0,
        height: 56.0,
      ),
      elevation: 6.0,
      disabledElevation: 6.0,
      onPressed: onPressed,
      child: Icon(icon),
    );
  }
}
