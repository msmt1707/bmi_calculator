import 'package:flutter/material.dart';

//Constants used across application
const kScaffoldBackgroundColor = Color(0xFF00AABB);

const kIconContent_iconSize = 80.0;
const kIconContent_fontSize = 18.0;

const kLabelTextStyle = TextStyle(
  fontSize: kIconContent_fontSize,
  color: Colors.white,
);

const kReusableCard_borderRadius = 10.0;
const kReusableCard_margin = 15.0;

const kBottomContainerColor = Colors.red;
const kBottomContainerHeight = 45.0;

const kInactiveCardColor = Color(0xFFAABBCC);
const kActiveCardColor = Color(0xFF00AABB);

const kNumberTextStyle = TextStyle(
  fontSize: 60.0,
  fontWeight: FontWeight.bold,
);

const double kSliderValueMin = 120.0;
const double kSliderValueMax = 220.0;
