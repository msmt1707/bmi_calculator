import 'package:flutter/material.dart';
import '../constants.dart';

class ReusableCard extends StatelessWidget {
  final Color color;
  final Widget cardChild;

  ReusableCard({@required this.color, this.cardChild});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: cardChild,
      margin: EdgeInsets.all(kReusableCard_margin),
      decoration: BoxDecoration(
        color: color, //Color(0xFFAABBCC),
        borderRadius: BorderRadius.circular(kReusableCard_borderRadius),
      ),
    );
  }
}
